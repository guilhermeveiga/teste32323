﻿namespace Conversor_Dados
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblArquivoMesAtual = new System.Windows.Forms.Label();
            this.btnGerar = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dtpDataLiberacao = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxComprador = new Conversor_Dados.Components_customizados.SIG_ComboBox();
            this.cbxEmpresa = new Conversor_Dados.Components_customizados.SIG_ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Conversor_Dados.Properties.Resources.login_;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(956, 73);
            this.panel1.TabIndex = 1;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Font = new System.Drawing.Font("Montserrat Medium", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(15, 20);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(219, 35);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Mex - > Operadora";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(17, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Empresa";
            // 
            // lblArquivoMesAtual
            // 
            this.lblArquivoMesAtual.AutoSize = true;
            this.lblArquivoMesAtual.BackColor = System.Drawing.Color.Transparent;
            this.lblArquivoMesAtual.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArquivoMesAtual.ForeColor = System.Drawing.Color.DimGray;
            this.lblArquivoMesAtual.Location = new System.Drawing.Point(739, 134);
            this.lblArquivoMesAtual.Name = "lblArquivoMesAtual";
            this.lblArquivoMesAtual.Size = new System.Drawing.Size(148, 21);
            this.lblArquivoMesAtual.TabIndex = 5;
            this.lblArquivoMesAtual.Text = "ARQUIVO MÊS ATUAL";
            this.lblArquivoMesAtual.Click += new System.EventHandler(this.lblArquivoMesAtual_Click);
            // 
            // btnGerar
            // 
            this.btnGerar.ActiveBorderThickness = 1;
            this.btnGerar.ActiveCornerRadius = 20;
            this.btnGerar.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.ActiveForecolor = System.Drawing.Color.White;
            this.btnGerar.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.BackColor = System.Drawing.Color.White;
            this.btnGerar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGerar.BackgroundImage")));
            this.btnGerar.ButtonText = "Gerar";
            this.btnGerar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerar.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.IdleBorderThickness = 1;
            this.btnGerar.IdleCornerRadius = 20;
            this.btnGerar.IdleFillColor = System.Drawing.Color.White;
            this.btnGerar.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.Location = new System.Drawing.Point(21, 320);
            this.btnGerar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(907, 35);
            this.btnGerar.TabIndex = 22;
            this.btnGerar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnGerar.Click += new System.EventHandler(this.btnGerar_Click);
            // 
            // dtpDataLiberacao
            // 
            this.dtpDataLiberacao.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dtpDataLiberacao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataLiberacao.Location = new System.Drawing.Point(743, 210);
            this.dtpDataLiberacao.Name = "dtpDataLiberacao";
            this.dtpDataLiberacao.Size = new System.Drawing.Size(185, 27);
            this.dtpDataLiberacao.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(739, 186);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 21);
            this.label4.TabIndex = 36;
            this.label4.Text = "Data da Liberação da Carga";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(17, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 21);
            this.label2.TabIndex = 37;
            this.label2.Text = "Comprador";
            // 
            // cbxComprador
            // 
            this.cbxComprador.FormattingEnabled = true;
            this.cbxComprador.ItemHeight = 22;
            this.cbxComprador.Items.AddRange(new object[] {
            "BTCC"});
            this.cbxComprador.Location = new System.Drawing.Point(21, 209);
            this.cbxComprador.Name = "cbxComprador";
            this.cbxComprador.Size = new System.Drawing.Size(693, 28);
            this.cbxComprador.TabIndex = 38;
            // 
            // cbxEmpresa
            // 
            this.cbxEmpresa.FormattingEnabled = true;
            this.cbxEmpresa.ItemHeight = 22;
            this.cbxEmpresa.Items.AddRange(new object[] {
            "OI",
            "PAGGO",
            "BRT",
            "BTCC",
            "SEREDE",
            "REDE CONECTA",
            "PACTUAL",
            "L2R"});
            this.cbxEmpresa.Location = new System.Drawing.Point(21, 127);
            this.cbxEmpresa.Name = "cbxEmpresa";
            this.cbxEmpresa.Size = new System.Drawing.Size(693, 28);
            this.cbxEmpresa.TabIndex = 4;
            this.cbxEmpresa.SelectedValueChanged += new System.EventHandler(this.cbxEmpresa_SelectedValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 373);
            this.Controls.Add(this.cbxComprador);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpDataLiberacao);
            this.Controls.Add(this.btnGerar);
            this.Controls.Add(this.lblArquivoMesAtual);
            this.Controls.Add(this.cbxEmpresa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label label1;
        private Components_customizados.SIG_ComboBox cbxEmpresa;
        private System.Windows.Forms.Label lblArquivoMesAtual;
        private Bunifu.Framework.UI.BunifuThinButton2 btnGerar;
        private System.Windows.Forms.DateTimePicker dtpDataLiberacao;
        private System.Windows.Forms.Label label4;
        private Components_customizados.SIG_ComboBox cbxComprador;
        private System.Windows.Forms.Label label2;
    }
}

