﻿using Global;
using System;
using System.Windows.Forms;

namespace Conversor_Dados
{
    public partial class Form1 : SombraAereo
    {
        private string sArquivo;

        clsOperadora cOperadora = new clsOperadora();
        clsManager cMng = new clsManager();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void cbxEmpresa_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            string empresa = cbxEmpresa.Text;
            switch (empresa)
            {
                case "OI":
                    {   //cOperadora.ProcuraOperadorasOI(sArquivo);
                        cMng.CreateLayoutPedidoCompraSETPS(sArquivo);       //  SETPS ( 45, 310 )
                        cMng.CreateLayoutPedidoCompraMETROPASSE(sArquivo);  // METROPASSE ( 73, 288 )
                        cMng.CreateLayoutPedidoCompraSETRANSBEL(sArquivo);  // SETRANSBEL ( 52 )                                                                            
                    }
                    break;
                case "PAGGO":
                    {
                        cMng.CreateLayoutPedidoCompraOtimo(sArquivo);       // OTIMO ( 55 )
                        cMng.CreateLayoutPedidoCompraSETPS(sArquivo);       // SETPS ( 45 )
                        cMng.CreateLayoutPedidoCompraMETROPASSE(sArquivo);  // METROPASSE( 73 )
                        cMng.CreateLayoutPedidoCompraBHBus(sArquivo);       // BHBUS ( 30 )
                        cMng.CreateLayoutPedidoCompraSETRANSBEL(sArquivo);  // SETRANSBEL ( 52 )
                        cMng.CreateLayoutPedidoCompraTRANSPAL(sArquivo);    // TRANSPAL ( 38 )
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017",sArquivo,"00000000000000");// RIOCARD - FETRANSPORT ( 36 )
                        cMng.CreateLayoutPedidoCompraSINETRAM(sArquivo);    // SINETRAM ( 49 )
                        // ASSETUR ( 209 )
                    }
                    break;
                case "BRT":
                    {
                        cMng.CreateLayoutPedidoCompraSETPS(sArquivo);       // SETPS ( 80 , 45 )
                        cMng.CreateLayoutPedidoCompraMETROPASSE(sArquivo);  // METROPASSE ( 107 )
                        cMng.CreateLayoutPedidoCompraBHBus(sArquivo);       // BHBUS
                        cMng.CreateLayoutPedidoCompraSETRANSBEL(sArquivo);  // SETRANSBEL
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017", sArquivo, "00000000000000");// RIOCARD - FETRANSPORT
                        cMng.CreateLayoutPedidoCompraSINETRAM(sArquivo);    // SINETRAM
                        // ASSETUR
                    }
                    break;
                case "BTCC":
                    {
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017", sArquivo, "00000000000000");// RIOCARD - FETRANSPORT
                        // ASSETUR
                    }
                    break;
                case "SEREDE":
                    {
                        cMng.CreateLayoutPedidoCompraSETPS(sArquivo);       // SETPS
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017", sArquivo, "00000000000000");// RIOCARD - FETRANSPORT
                    }
                    break;
                case "REDE CONECTA":
                    {
                        cMng.CreateLayoutPedidoCompraSETPS(sArquivo);       // SETPS
                        cMng.CreateLayoutPedidoCompraMETROPASSE(sArquivo);  // METROPASSE
                        cMng.CreateLayoutPedidoCompraSETRANSBEL(sArquivo);  // SETRANSBEL
                        cMng.CreateLayoutPedidoCompraTRANSPAL(sArquivo);    // TRANSPAL
                        cMng.CreateLayoutPedidoCompraSINETRAM(sArquivo);    // SINETRAM
                    }
                    break;
                case "PACTUAL":
                    {
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017", sArquivo, "00000000000000");// RIOCARD - FETRANSPORT
                    }
                    break;
                case "L2R":
                    {
                        cMng.CreateLayoutPedidoCompraMETROPASSE(sArquivo);  // METROPASSE
                        cMng.CreateLayoutPedidoCompraBHBus(sArquivo);       // BHBUS
                        cMng.CreateLayoutPedidoCompraFETRANSPORT("12102017", sArquivo, "00000000000000");// RIOCARD - FETRANSPORT
                    }
                    break;
                default: { } break;
            }
        }

        private void lblArquivoMesAtual_Click(object sender, EventArgs e)
        {
            string empresa = cbxEmpresa.Text;
            //if (empresa == "BRT"){   }
            OpenFileDialog ofd1 = new OpenFileDialog();

            ofd1.Multiselect = true;
            ofd1.Title = "Selecione um Arquivo";
            ofd1.InitialDirectory = @"C:\";
            ofd1.Filter = "Excel (*.xlsx)|*.xlsx";
            ofd1.CheckFileExists = true;
            ofd1.CheckPathExists = true;
            ofd1.FilterIndex = 2;
            ofd1.RestoreDirectory = true;
            ofd1.ReadOnlyChecked = true;
            ofd1.ShowReadOnly = true;

            if (ofd1.ShowDialog() == DialogResult.OK)
            {
                sArquivo = ofd1.FileName.ToString();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
