﻿using Global;
using OfficeOpenXml;
using System;
using System.IO;

namespace Conversor_Dados
{
    public class clsManager
    {
        clsCriaTexto texto = new clsCriaTexto();

        public void Layout(string NomeEmpresa, string DataEntrega, string CaminhoArquivo) // Função responsavel por gerenciar qual layout utilizar
        {
            switch (NomeEmpresa)
            {

                case "OI":
                    //FetransportPedido(DataEntrega, CaminhoArquivo);
                    break;
                case "BTCC":
                    //LayoutsBTCC();
                    break;
                case "SEREDE":
                    //LayoutsSEREDE();
                    break;
                case "BRT":
                    //LayoutsBRT();
                    break;
                case "L2R":
                    //LayoutsL2R();
                    break;
                case "GUANABARA":
                    //LayoutsGUANABARA();
                    break;
                default:
                    break;
            }
        }

        /* OTIMO
        Utilize o bloco de notas e crie um arquivo "importacao.txt" contendo os seguintes campos separados por ponto e virgula (;), Matrícula;Nome Usuário;Valor; onde:
        Matrícula	Nome Usuário	Valor
        Matrícula: Campo de identificação do usuário na Empresa.	
        Nome Usuário: Campo que será utilizado apenas se o usuário não for localizado pelo sistema, durante o processo de importação.	
        Valor: Campo numérico com o valor a ser recarregado para o usuário máscara ###,##
        087657A2	Fabricio de Almeida Prado	23,15
        Exemplo:
        01235;Frederico de Andrade;10,00;
        2154;Pedro Paulo Santos;11,21;
        4568;Claudia Renata Maria;45,01;
        A871;Heleno Victor Cruz;89,15;
        Obs:É importante que no final de cada linha após o campo valor tenha um ponto e virgula (;)
    */
        public void CreateLayoutPedidoCompraOtimo(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "importacao.txt";

            decimal soma_tudo;
            decimal soma_tudo2 = 0;
            string Vl_carga_body_divisao;

            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                string Matricula = workSheet.Cells[i, 15].Value.ToString().PadRight(15, ' ');
                string NomeCompleto = workSheet.Cells[i, 16].Value.ToString();
                string Valor_carga = workSheet.Cells[i, 39].Value.ToString();

                // não lembro o pq disso !!
                if (Operadora == null)
                {
                    return;
                }
                if (Operadora.ToString() == "OTIMO")
                {
                    if (Valor_carga.Length > 10)
                    {
                        string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", ",");
                        Valor_carga = variavel;
                    }

                    soma_tudo = Decimal.Round(Convert.ToDecimal(Valor_carga), 2);
                    soma_tudo2 = soma_tudo2 + soma_tudo;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }

                    Valor_carga = Valor_carga.Replace(",", "").PadLeft(5, '0');
                    texto.Insert(Matricula + ";" + NomeCompleto + ";" + Valor_carga + ";", caminhoSaidaFinal);
                }
            }
        }

        // fazer testes com o pessoal do outro lado
        /* SETPS
        .: O arquivo de importação de pedido de carga deverá conter os valores para carregamento dos funcionários usuários de Vale Transporte.

        .: O arquivo será padrão TXT sem cabeçalho com campos de tamanho fixo, sendo um registro por linha com a seguinte estrutura:

        Campo Tipo    Tamanho Posição Formato
        MATRICULA   Caracter    15	001-015	
        VALOR PARA CARREGAMENTO Numérico	5	016-021	2 últimas posições centavos, sem separação

        .: O valor para carregamento deve estar compreendido entre R$ 0,00 e R$ 650,00.
        */
        public void CreateLayoutPedidoCompraSETPS(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_SETPS.txt";

            string Vl_carga_body_divisao;

            int iCount = 0;
            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula2 = workSheet.Cells[i, 15].Value;
                string Matricula = "";
                object Valor_carga2 = workSheet.Cells[i, 39].Value;
                string Valor_carga = "";

                if (Matricula2 != null)
                {
                    Matricula = Matricula2.ToString().PadRight(15, ' ');
                }

                if (Valor_carga2 != null)
                {
                    Valor_carga = Valor_carga2.ToString();
                }

                // Gamba adicionada para não dar erro de NULL no OBJETO ( RETIRAR ISSO LOGO !!!! )
                if (Operadora == null)
                {
                    return;
                }

                if (Operadora.ToString() == "SETPS" || Operadora.ToString() == "45" || Operadora.ToString() == "310" || Operadora.ToString() == "85") // Em algumas planilhas SETPS = VTEONLINE
                {
                    string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", "");
                    Valor_carga = variavel;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }
                    else if (Valor_carga.IndexOf(",") < 0)
                    {
                        Valor_carga = Valor_carga + ",00";
                    }

                    Valor_carga = Valor_carga.Replace(",", "").PadLeft(5, ' ');
                    texto.Insert(Matricula + Valor_carga, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }

        /* METROPASSE
        .: O arquivo de importação de pedido de carga deverá conter os valores para carregamento dos funcionários usuários de Vale Transporte.

        .: O arquivo será padrão TXT sem cabeçalho com campos de tamanho fixo, sendo um registro por linha com a seguinte estrutura:

        Campo Tipo    Tamanho Posição Formato
        MATRICULA   Caracter    15	001-015	
        VALOR PARA CARREGAMENTO Numérico	5	016-021	2 últimas posições centavos, sem separação

        .: O valor para carregamento deve estar compreendido entre R$ 0,00 e R$ 650,00.
        */
        public void CreateLayoutPedidoCompraMETROPASSE(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_METROPASSE.txt";

            string Vl_carga_body_divisao;

            int iCount = 0;
            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula2 = workSheet.Cells[i, 15].Value;
                string Matricula = "";
                object Valor_carga2 = workSheet.Cells[i, 39].Value;
                string Valor_carga = "";

                if (Matricula2 != null)
                {
                    Matricula = Matricula2.ToString().PadRight(15, ' ');
                }

                if (Valor_carga2 != null)
                {
                    Valor_carga = Valor_carga2.ToString();
                }

                // Gamba adicionada para não dar erro de NULL no OBJETO ( RETIRAR ISSO LOGO !!!! )
                if (Operadora == null)
                {
                    return;
                }

                if (Operadora.ToString() == "METROPASSE" || Operadora.ToString() == "73" || Operadora.ToString() == "288" || Operadora.ToString() == "107")
                {
                    string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", "");
                    Valor_carga = variavel;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }
                    else if (Valor_carga.IndexOf(",") < 0)
                    {
                        Valor_carga = Valor_carga + ",00";
                    }

                    Valor_carga = Valor_carga.Replace(",", "").PadLeft(5, ' ');
                    texto.Insert(Matricula + Valor_carga, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }

        /* BHBUS
        .: O arquivo de importação de pedido de carga deverá conter os valores para carregamento dos funcionários usuários de Vale Transporte.

        .: O arquivo será padrão TXT sem cabeçalho com campos de tamanho fixo, sendo um registro por linha com a seguinte estrutura:

        Campo	                    Tipo	    Tamanho	    Posição	Formato
        MATRICULA	                Caracter	15	        001-015	
        VALOR PARA CARREGAMENTO	    Numérico	5	        016-020	2 últimas posições centavos, sem separação

        .: O valor para carregamento deve estar compreendido entre R$ 0,00 e R$ 650,00.

         */
        public void CreateLayoutPedidoCompraBHBus(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_BHBus.txt";

            string Vl_carga_body_divisao;

            int iCount = 0;
            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula2 = workSheet.Cells[i, 15].Value;
                string Matricula = "";
                object Valor_carga2 = workSheet.Cells[i, 39].Value;
                string Valor_carga = "";

                if (Matricula2 != null)
                {
                    Matricula = Matricula2.ToString().PadRight(15, ' ');
                }

                if (Valor_carga2 != null)
                {
                    Valor_carga = Valor_carga2.ToString();
                }

                // Gamba adicionada para não dar erro de NULL no OBJETO ( RETIRAR ISSO LOGO !!!! )
                if (Operadora == null)
                {
                    return;
                }

                if (Operadora.ToString() == "BHBUS")
                {
                    string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", "");
                    Valor_carga = variavel;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }
                    else if (Valor_carga.IndexOf(",") < 0)
                    {
                        Valor_carga = Valor_carga + ",00";
                    }

                    Valor_carga = Valor_carga.Replace(",", "").PadLeft(5, ' ');
                    texto.Insert(Matricula + Valor_carga, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }

        /* SETRANSBEL
            Importar  >> Instruções
            Instruções para gerar um arquivo para inserção de Pedidos 

            Primeira Linha/Registro
            Inicio	Fim	    Valor da Coluna	    Obrigatório
            1	    4	    versão(valor 0200)	Sim

             Demais Linhas/Registros 
            Separador	Valor da Coluna	    Obrigatório	    Observação
            "|"	        CPF do usuário	    Sim	            CPF do usuário a ser feita a recarga
            "|"	        Quantidade de dias	Sim	            Quantidade de dias úteis no mês para este usuário
            "|"	        Valor de uso diário	Sim	            Valor de uso diário para este usuário(sem pontos ou vírgulas)
            "|"	        Nome do usuário	    Não	            Nome do usuário a ser feita a recarga


            Exemplo do arquivo:
            LINHA 1: 0200 
            LINHA 2: 12345678900|22|1000|JOSÉ DA SILVA
            LINHA 3: 12345678901|20|800|MARIA JESUS
        */
        public void CreateLayoutPedidoCompraSETRANSBEL(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_SETRANSBEL.txt";
            string versaoLayout = "0200";

            texto.Insert(versaoLayout, caminhoSaidaFinal); // header

            string Vl_carga_body_divisao;

            int iCount = 0;

            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula = workSheet.Cells[i, 15].Value;
                object CPF = workSheet.Cells[i, 18].Value;
                object QuatidadeDias = workSheet.Cells[i, 27].Value;
                object QuantidadeValor2 = workSheet.Cells[i, 28].Value;
                object Nome = workSheet.Cells[i, 16].Value;

                // não lembro o pq disso !!
                if (Operadora == null)
                {
                    return;
                }
                if (Matricula == null)
                {
                    return;
                }
                if (CPF == null)
                {
                    return;
                }
                if (QuatidadeDias == null)
                {
                    return;
                }
                if (QuantidadeValor2 == null)
                {
                    return;
                }
                if (Nome == null)
                {
                    return;
                }

                Matricula = workSheet.Cells[i, 15].Value.ToString();
                CPF = workSheet.Cells[i, 18].Value.ToString();
                string QuantidadeValor = QuantidadeValor2.ToString();
                QuatidadeDias = workSheet.Cells[i, 27].Value.ToString();// 27
                QuantidadeValor = workSheet.Cells[i, 28].Value.ToString(); // 28
                Nome = workSheet.Cells[i, 16].Value.ToString(); // 16

                if (Operadora.ToString() == "SETRANSOL")
                {
                    if (QuantidadeValor.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = QuantidadeValor.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            QuantidadeValor = QuantidadeValor + "0";
                        }
                    }
                    else if (QuantidadeValor.IndexOf(",") < 0)
                    {
                        QuantidadeValor = QuantidadeValor + ",00";
                    }

                    QuantidadeValor = QuantidadeValor.Replace(",", "");
                    texto.Insert(CPF + "|" + QuatidadeDias + "|" + QuantidadeValor + "|" + Nome, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }

        /* TRANSPAL
            Importar  >> Instruções
            Instruções para gerar um arquivo para inserção de Pedidos 

            Primeira Linha/Registro
            Inicio	Fim	    Valor da Coluna	    Obrigatório
            1	    4	    versão(valor 0200)	Sim

             Demais Linhas/Registros 
            Separador	Valor da Coluna	    Obrigatório	    Observação
            "|"	        CPF do usuário	    Sim	            CPF do usuário a ser feita a recarga
            "|"	        Quantidade de dias	Sim	            Quantidade de dias úteis no mês para este usuário
            "|"	        Valor de uso diário	Sim	            Valor de uso diário para este usuário(sem pontos ou vírgulas)
            "|"	        Nome do usuário	    Não	            Nome do usuário a ser feita a recarga


            Exemplo do arquivo:
            LINHA 1: 0200 
            LINHA 2: 12345678900|22|1000|JOSÉ DA SILVA
            LINHA 3: 12345678901|20|800|MARIA JESUS
        */
        public void CreateLayoutPedidoCompraTRANSPAL(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_TRANSPAL.txt";
            string versaoLayout = "0200";

            texto.Insert(versaoLayout, caminhoSaidaFinal); // header

            string Vl_carga_body_divisao;

            int iCount = 0;

            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula = workSheet.Cells[i, 15].Value;
                object CPF = workSheet.Cells[i, 18].Value;
                object QuatidadeDias = workSheet.Cells[i, 27].Value;
                object QuantidadeValor2 = workSheet.Cells[i, 28].Value;
                object Nome = workSheet.Cells[i, 16].Value;

                // não lembro o pq disso !!
                if (Operadora == null)
                {
                    return;
                }
                if (Matricula == null)
                {
                    return;
                }
                if (CPF == null)
                {
                    return;
                }
                if (QuatidadeDias == null)
                {
                    return;
                }
                if (QuantidadeValor2 == null)
                {
                    return;
                }
                if (Nome == null)
                {
                    return;
                }

                Matricula = workSheet.Cells[i, 15].Value.ToString();
                CPF = workSheet.Cells[i, 18].Value.ToString();
                string QuantidadeValor = QuantidadeValor2.ToString();
                QuatidadeDias = workSheet.Cells[i, 27].Value.ToString();// 27
                QuantidadeValor = workSheet.Cells[i, 28].Value.ToString(); // 28
                Nome = workSheet.Cells[i, 16].Value.ToString(); // 16

                if (Operadora.ToString() == "TRANSPAL")
                {
                    if (QuantidadeValor.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = QuantidadeValor.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            QuantidadeValor = QuantidadeValor + "0";
                        }
                    }
                    else if (QuantidadeValor.IndexOf(",") < 0)
                    {
                        QuantidadeValor = QuantidadeValor + ",00";
                    }

                    QuantidadeValor = QuantidadeValor.Replace(",", "");
                    texto.Insert(CPF + "|" + QuatidadeDias + "|" + QuantidadeValor + "|" + Nome, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }

        /* FETRANSPORT
        Registro Tipo 01 - Header do Arquivo
        Nome            Formato     Tam.    Comentários
        Nr_seq_reg      Num         5       Número de seqüência do registro. Iniciado em 1, para cada arquivo.
        Tp_registro     Num         2       Tipo do Registro: 01 - Header do arquivo.
        Nm_arquivo      Alfa        6       Nome do arquivo = "PEDIDO". Constante que identifica o arquivo.
        Nr_versão       Alfa        5       Número da versão do layout do arquivo. Versão = 01.00
        Nr_doc_comprd   Num         14      Número do CPF, CNPJ ou CEI do comprador.  

        Registro Tipo 02 – Item do Pedido 
        Nome            Formato     Tam.    Comentários
        Nr_seq_reg      Num         5       Número de seqüência do registro.
        Tp_registro     Num         2       Tipo de Registro: 02 – Item do Pedido
        Nr_matrícula    Alfa        15      Número da matrícula do usuário.
        Vl_carga        Num         8       Valor da carga, em centavos. Alinhado à direita, completado com zeros

        Registro Tipo 03 – Dados da Entrega/Retirada dos Cartões
        Este registro é opcional, devendo ser informado caso não utilize o site para a finalização do pedido.
        Nome            Formato     Tam.    Comentários
        Nr_seq_reg      Num         5       Número de seqüência do registro.
        Tp_registro     Num         2       Tipo de Registro:   03 – Dados da Entrega/Retirada dos cartões
        Dt_liber_carga  Num         8       Data da liberação da carga.  Informar caso se queira fixar uma data de liberação maior do que os 5 ou 7 dias úteis após o pagamento. Formato ddmmaaaa.
        Tp_entrega      Alfa        1       Tipo da entrega do cartão.
        Informar caso o pedido tenha solicitações de cartões novos.
        "D" - Entrega domiciliar (será calculada uma tarifa de entrega
        baseada na quantidade de cartões).
        "A" - Retirada na agência do Unibanco informada abaixo.

        Nr_agência      Num         4       Número da agência para retirada dos cartões produzidos junto com o pedido.
        Deverá constar na relação de agências do Unibanco que
        fazem a entrega de cartões. 

        Registro Tipo 99 – Fim de Arquivo
        Nome            Formato     Tam.    Comentários 
        Nr_seq_reg      Num         5       Número de seqüência do registro.
        Tp_registro     Num         2       Tipo do Registro: 99 - Fim de Arquivo
        Vl_pedido       Num         10      Valor total do pedido, em centavos.
        Alinhado à direita, completado com zeros à esquerda.
        Exemplo: R$ 1.852,45  0000185245


        2.1 Regras de Geração
        O nome externo do arquivo será composto por:
        • Constante “PEDIDO”;
        • Número da versão do arquivo, com 4 posições (sem o ponto);
        • CPF/CNPJ/CEI do cliente comprador, com 14 posições;
        • Data de geração do arquivo, no formato aaaammdd;
        • Hora de geração do arquivo, no formato hhmm;
        • Separados por underscore “_”.
        Exemplo para um arquivo gerado para um cliente com o Cnpj de nr.º 22.333.444/0001-55, em 12/01/2005,
        às 15:25:
        “PEDIDO_0100_223334440000155_20050112_1525.txt”. 
        */
        public void CreateLayoutPedidoCompraFETRANSPORT(string DataEntrega, string caminhoEntrada, string CNPJComprador, string NomeArquivo = "PEDIDO", string TipoEntrega = "A", string NumeroAgencia = "7777", string NumeroVersao = "01.00")
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string nomeArquivo = NomeArquivo + "_" + NumeroVersao.Replace(".", "") + "_" + CNPJComprador + "_" + DateTime.Today.ToString("yyyy-MM-dd").Replace("-", "") + "_" + DateTime.Today.Hour.ToString("HH:mm").Replace(":", "") + "_Fetransport.txt";
            string caminhoSaidaFinal = globalDefs.caminhoSaida + nomeArquivo;

            string sequencia = "00001";
            string tipo_registro_header = "01";
            string tipo_registro_corpo = "02";
            string tipo_registro_footer = "03";
            string tipo_registro_fim = "99";

            decimal soma_tudo;
            decimal soma_tudo2 = 0;
            string soma_tudo3;
            string Vl_carga_body_divisao;
            int Nr_seq_reg_body = 1; // Num 5 Número de seqüência do registro.
            string Nr_seq_reg_body_2;
            string Nr_seq_reg_body_3;
            string Nr_seq_reg_body_4;

            texto.Insert(sequencia + tipo_registro_header + NomeArquivo + NumeroVersao + CNPJComprador, caminhoSaidaFinal); //criando Header

            int iSequencial = 2;

            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula = workSheet.Cells[i, 15].Value;
                object Valor_carga2 = workSheet.Cells[i, 39].Value;
                if (Operadora == null)
                {
                    return;
                }
                if (Matricula == null)
                {
                    return;
                }
                if (Valor_carga2 == null)
                {
                    return;
                }

                Matricula = Matricula.ToString().PadRight(15, ' ');
                string Valor_carga = Valor_carga2.ToString();


                if (Operadora.ToString() == "1" || Operadora.ToString() == "FETRANSPORT" || Operadora.ToString() == "RIOCARD")
                {
                    if (Valor_carga.Length > 10)
                    {
                        string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", ",");
                        Valor_carga = variavel;
                    }

                    soma_tudo = Decimal.Round(Convert.ToDecimal(Valor_carga), 2);
                    soma_tudo2 = soma_tudo2 + soma_tudo;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }

                    Valor_carga = Valor_carga.Replace(",", "").PadLeft(8, '0');
                    Nr_seq_reg_body++;

                    Nr_seq_reg_body_2 = Nr_seq_reg_body.ToString().PadLeft(5, '0');

                    texto.Insert(Nr_seq_reg_body_2 + tipo_registro_corpo + Matricula + Valor_carga, caminhoSaidaFinal);
                    Console.WriteLine("[ " + Nr_seq_reg_body.ToString() + " ] RIO CARD - Matricula[" + Matricula + "]-" + "Valor[" + Valor_carga + "]");

                    iSequencial++;
                }
            }
            Nr_seq_reg_body++;
            Nr_seq_reg_body_3 = Nr_seq_reg_body.ToString().PadLeft(5, '0');
            texto.Insert(Nr_seq_reg_body_3 + tipo_registro_footer + DataEntrega + TipoEntrega + NumeroAgencia, caminhoSaidaFinal); //criando footer


            Nr_seq_reg_body++;
            Nr_seq_reg_body_4 = Nr_seq_reg_body.ToString().PadLeft(5, '0');

            soma_tudo3 = soma_tudo2.ToString().Replace(",", "");
            soma_tudo3 = soma_tudo3.PadLeft(10, '0');

            texto.Insert(Nr_seq_reg_body_4 + tipo_registro_fim + soma_tudo3, caminhoSaidaFinal);
            Console.WriteLine("FOOTER2 - Linhas[" + Nr_seq_reg_body_4 + "] - Valor Geral [ " + soma_tudo3 + " ]");
        }

        /* SINETRAM 
         Cada linha do arquivo de importação corresponde à transferência de créditos para um funcionário.
         Veja abaixo a descrição de cada campo.
         MATRÍCULA: É a matricula do Funcionário na Empresa.
         • O código é Alfanumérico;
         • Deve conter no máximo 20 dígitos;
         • O funcionário já deve estar cadastrado no sistema e ativo;
         • Exemplo: 123456.
         VALOR (R$): É o valor em Reais que será transferido para o funcionário.
         • O valor máximo é de R$ 500,00;
         • Exemplo: 120.00. 
         Atenção! No campo “Valor (R$)” deve ser
         preenchido o separador de centavos com “Ponto”
         (120.00), e não com vírgulas (120,00).
        */
        public void CreateLayoutPedidoCompraSINETRAM(string caminhoEntrada)
        {
            FileInfo newFile = new FileInfo(caminhoEntrada);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

            string caminhoSaidaFinal = globalDefs.caminhoSaida + "_SINETRAM.txt";

            string Vl_carga_body_divisao;

            int iCount = 0;
            for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Operadora = workSheet.Cells[i, 26].Value;
                object Matricula2 = workSheet.Cells[i, 15].Value;
                //string Matricula = "";
                object Valor_carga2 = workSheet.Cells[i, 39].Value;
                string Valor_carga = "";
                if (Operadora == null)
                {
                    return;
                }

                if (Valor_carga2 == null)
                {
                    return;
                }

                if (Operadora.ToString() == "SINETRAM")
                {
                    string variavel = Decimal.Round(Convert.ToDecimal(Valor_carga), 2).ToString().Replace(".", "");
                    Valor_carga = variavel;

                    if (Valor_carga.IndexOf(",") > 0)
                    {
                        Vl_carga_body_divisao = Valor_carga.ToString().Split(',')[1];

                        if (Vl_carga_body_divisao.Length == 1)
                        {
                            Valor_carga = Valor_carga + "0";
                        }
                    }
                    else if (Valor_carga.IndexOf(",") < 0)
                    {
                        Valor_carga = Valor_carga + ",00";
                    }

                    Valor_carga = Valor_carga.Replace(",", ".").PadLeft(5, ' ');

                    texto.Insert(Matricula2 + ";" + Valor_carga2, caminhoSaidaFinal);
                    iCount++;
                }
            }
        }
    }    
}
