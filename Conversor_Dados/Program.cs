﻿using Conversor.Telas.ArquivoRetorno;
using System;
using System.Windows.Forms;

namespace Conversor_Dados
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());// frmArquivoRetorno());
        }
    }
}
