﻿namespace Conversor_Dados
{
    partial class frmEnviaServidor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnviaServidor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnGerar = new Bunifu.Framework.UI.BunifuThinButton2();
            this.cbxEmpresa = new Conversor_Dados.Components_customizados.SIG_ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblArquivoMesAtual = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Conversor_Dados.Properties.Resources.login_;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Location = new System.Drawing.Point(1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(809, 73);
            this.panel1.TabIndex = 2;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Font = new System.Drawing.Font("Montserrat Medium", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(15, 20);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(194, 35);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Mex - > Servidor";
            // 
            // btnGerar
            // 
            this.btnGerar.ActiveBorderThickness = 1;
            this.btnGerar.ActiveCornerRadius = 20;
            this.btnGerar.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.ActiveForecolor = System.Drawing.Color.White;
            this.btnGerar.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.BackColor = System.Drawing.Color.White;
            this.btnGerar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGerar.BackgroundImage")));
            this.btnGerar.ButtonText = "Enviar";
            this.btnGerar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerar.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.IdleBorderThickness = 1;
            this.btnGerar.IdleCornerRadius = 20;
            this.btnGerar.IdleFillColor = System.Drawing.Color.White;
            this.btnGerar.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.btnGerar.Location = new System.Drawing.Point(105, 299);
            this.btnGerar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(573, 35);
            this.btnGerar.TabIndex = 23;
            this.btnGerar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxEmpresa
            // 
            this.cbxEmpresa.FormattingEnabled = true;
            this.cbxEmpresa.ItemHeight = 22;
            this.cbxEmpresa.Items.AddRange(new object[] {
            "OI",
            "PAGGO",
            "BRT",
            "BTCC",
            "SEREDE",
            "REDE CONECTA",
            "PACTUAL",
            "L2R"});
            this.cbxEmpresa.Location = new System.Drawing.Point(40, 173);
            this.cbxEmpresa.Name = "cbxEmpresa";
            this.cbxEmpresa.Size = new System.Drawing.Size(412, 28);
            this.cbxEmpresa.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(36, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 21);
            this.label1.TabIndex = 24;
            this.label1.Text = "Empresa";
            // 
            // lblArquivoMesAtual
            // 
            this.lblArquivoMesAtual.AutoSize = true;
            this.lblArquivoMesAtual.BackColor = System.Drawing.Color.Transparent;
            this.lblArquivoMesAtual.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArquivoMesAtual.ForeColor = System.Drawing.Color.DimGray;
            this.lblArquivoMesAtual.Location = new System.Drawing.Point(598, 91);
            this.lblArquivoMesAtual.Name = "lblArquivoMesAtual";
            this.lblArquivoMesAtual.Size = new System.Drawing.Size(177, 21);
            this.lblArquivoMesAtual.TabIndex = 26;
            this.lblArquivoMesAtual.Text = "ARQUIVO A SER ENVIADO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(500, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 21);
            this.label2.TabIndex = 27;
            this.label2.Text = "Competência";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(504, 180);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(271, 20);
            this.dateTimePicker1.TabIndex = 28;
            // 
            // frmEnviaServidor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 347);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblArquivoMesAtual);
            this.Controls.Add(this.cbxEmpresa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGerar);
            this.Controls.Add(this.panel1);
            this.Name = "frmEnviaServidor";
            this.Text = "frmEnviaServidor";
            this.Load += new System.EventHandler(this.frmEnviaServidor_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.Framework.UI.BunifuThinButton2 btnGerar;
        private Components_customizados.SIG_ComboBox cbxEmpresa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblArquivoMesAtual;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}