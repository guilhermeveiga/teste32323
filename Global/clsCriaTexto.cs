﻿using System;

namespace Global
{
    public class clsCriaTexto
    {
        public void Insert(string texto, string caminhoSaida)
        {
            try
            {
                if (!System.IO.File.Exists(caminhoSaida))
                {
                    using (System.IO.StreamWriter sw = System.IO.File.CreateText(caminhoSaida))
                    {
                        sw.WriteLine(texto);
                    }
                    return;
                }
                else
                {
                    using (System.IO.StreamWriter sw = System.IO.File.AppendText(caminhoSaida))
                    {
                        sw.WriteLine(texto);
                    }
                    return;
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
