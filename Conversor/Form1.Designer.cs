﻿namespace Conversor
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblArquivoMesAnterior = new System.Windows.Forms.Label();
            this.lblArquivoMesAtual = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblRazaoSocial = new System.Windows.Forms.Label();
            this.lblTituloDois = new System.Windows.Forms.Label();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblNomeFantasia = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.siG_ComboBox1 = new Conversor.Components_customizados.SIG_ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Conversor.Properties.Resources.login_;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Location = new System.Drawing.Point(-3, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(956, 73);
            this.panel1.TabIndex = 0;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lblTitulo.Font = new System.Drawing.Font("Montserrat Medium", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(15, 20);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(199, 35);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Empresa - > Mex";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(14, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Empresa";
            // 
            // lblArquivoMesAnterior
            // 
            this.lblArquivoMesAnterior.AutoSize = true;
            this.lblArquivoMesAnterior.BackColor = System.Drawing.Color.Transparent;
            this.lblArquivoMesAnterior.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArquivoMesAnterior.ForeColor = System.Drawing.Color.DimGray;
            this.lblArquivoMesAnterior.Location = new System.Drawing.Point(584, 131);
            this.lblArquivoMesAnterior.Name = "lblArquivoMesAnterior";
            this.lblArquivoMesAnterior.Size = new System.Drawing.Size(174, 21);
            this.lblArquivoMesAnterior.TabIndex = 3;
            this.lblArquivoMesAnterior.Text = "ARQUIVO MÊS ANTERIOR";
            this.lblArquivoMesAnterior.Click += new System.EventHandler(this.lblArquivoMesAnterior_Click);
            // 
            // lblArquivoMesAtual
            // 
            this.lblArquivoMesAtual.AutoSize = true;
            this.lblArquivoMesAtual.BackColor = System.Drawing.Color.Transparent;
            this.lblArquivoMesAtual.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArquivoMesAtual.ForeColor = System.Drawing.Color.DimGray;
            this.lblArquivoMesAtual.Location = new System.Drawing.Point(779, 131);
            this.lblArquivoMesAtual.Name = "lblArquivoMesAtual";
            this.lblArquivoMesAtual.Size = new System.Drawing.Size(148, 21);
            this.lblArquivoMesAtual.TabIndex = 4;
            this.lblArquivoMesAtual.Text = "ARQUIVO MÊS ATUAL";
            this.lblArquivoMesAtual.Click += new System.EventHandler(this.lblArquivoMesAtual_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(89, 243);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 21);
            this.label4.TabIndex = 5;
            this.label4.Text = "CNPJ:";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.BackColor = System.Drawing.Color.Transparent;
            this.lblCNPJ.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblCNPJ.ForeColor = System.Drawing.Color.DimGray;
            this.lblCNPJ.Location = new System.Drawing.Point(144, 243);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(151, 21);
            this.lblCNPJ.TabIndex = 6;
            this.lblCNPJ.Text = "00.000.000/0000-00";
            this.lblCNPJ.Click += new System.EventHandler(this.lblCNPJ_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(25, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 21);
            this.label6.TabIndex = 7;
            this.label6.Text = "RAZÃO SOCIAL:";
            // 
            // lblRazaoSocial
            // 
            this.lblRazaoSocial.AutoSize = true;
            this.lblRazaoSocial.BackColor = System.Drawing.Color.Transparent;
            this.lblRazaoSocial.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblRazaoSocial.ForeColor = System.Drawing.Color.DimGray;
            this.lblRazaoSocial.Location = new System.Drawing.Point(144, 315);
            this.lblRazaoSocial.Name = "lblRazaoSocial";
            this.lblRazaoSocial.Size = new System.Drawing.Size(28, 21);
            this.lblRazaoSocial.TabIndex = 8;
            this.lblRazaoSocial.Text = "N/I";
            // 
            // lblTituloDois
            // 
            this.lblTituloDois.AutoSize = true;
            this.lblTituloDois.BackColor = System.Drawing.Color.Transparent;
            this.lblTituloDois.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblTituloDois.ForeColor = System.Drawing.Color.DimGray;
            this.lblTituloDois.Location = new System.Drawing.Point(13, 190);
            this.lblTituloDois.Name = "lblTituloDois";
            this.lblTituloDois.Size = new System.Drawing.Size(207, 21);
            this.lblTituloDois.TabIndex = 11;
            this.lblTituloDois.Text = "DADOS DO ARQUIVO VIGENTE";
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.bunifuSeparator1.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 2;
            this.bunifuSeparator1.Location = new System.Drawing.Point(18, 207);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(907, 14);
            this.bunifuSeparator1.TabIndex = 12;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // lblNomeFantasia
            // 
            this.lblNomeFantasia.AutoSize = true;
            this.lblNomeFantasia.BackColor = System.Drawing.Color.Transparent;
            this.lblNomeFantasia.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblNomeFantasia.ForeColor = System.Drawing.Color.DimGray;
            this.lblNomeFantasia.Location = new System.Drawing.Point(144, 280);
            this.lblNomeFantasia.Name = "lblNomeFantasia";
            this.lblNomeFantasia.Size = new System.Drawing.Size(28, 21);
            this.lblNomeFantasia.TabIndex = 14;
            this.lblNomeFantasia.Text = "N/I";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(82, 280);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 21);
            this.label9.TabIndex = 13;
            this.label9.Text = "NOME:";
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.White;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Gerar";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Location = new System.Drawing.Point(18, 374);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(907, 35);
            this.bunifuThinButton21.TabIndex = 21;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // siG_ComboBox1
            // 
            this.siG_ComboBox1.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.siG_ComboBox1.FormattingEnabled = true;
            this.siG_ComboBox1.ItemHeight = 22;
            this.siG_ComboBox1.Items.AddRange(new object[] {
            "OI",
            "PAGGO",
            "BRT",
            "BTCC",
            "SEREDE",
            "REDE CONECTA",
            "PACTUAL",
            "L2R"});
            this.siG_ComboBox1.Location = new System.Drawing.Point(18, 124);
            this.siG_ComboBox1.Name = "siG_ComboBox1";
            this.siG_ComboBox1.Size = new System.Drawing.Size(550, 28);
            this.siG_ComboBox1.TabIndex = 1;
            this.siG_ComboBox1.SelectedValueChanged += new System.EventHandler(this.siG_ComboBox1_SelectedValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(949, 434);
            this.Controls.Add(this.lblTituloDois);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.lblNomeFantasia);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.lblRazaoSocial);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCNPJ);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblArquivoMesAtual);
            this.Controls.Add(this.lblArquivoMesAnterior);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.siG_ComboBox1);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTitulo;
        private Components_customizados.SIG_ComboBox siG_ComboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblArquivoMesAnterior;
        private System.Windows.Forms.Label lblArquivoMesAtual;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTituloDois;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        public System.Windows.Forms.Label lblCNPJ;
        public System.Windows.Forms.Label lblRazaoSocial;
        public System.Windows.Forms.Label lblNomeFantasia;
    }
}

