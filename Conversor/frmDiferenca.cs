﻿using Conversor.Manager;
using Global;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using Conversor.Components_customizados;

namespace Conversor
{
    public partial class frmDiferenca : SombraAereo
    {
        List<clsNovosColaboradores> new_clsNovosColaboradores = new List<clsNovosColaboradores>();
        bool GerouExcel = false;

        public frmDiferenca(List<clsNovosColaboradores> customers, bool sucessoDados)
        {
            InitializeComponent();

            //if(sucessoDados == true )
            //{
            //    frmMessage fMessage = new frmMessage("[EXCEL] - Dados criado com sucesso !");
            //    fMessage.Show();     
            //}

            new_clsNovosColaboradores = customers;

            int iCount = new_clsNovosColaboradores.Count;

            for (int i = 0; i < iCount; i++)
            {
                bunifuCustomDataGrid1.Rows.Add(
                    new_clsNovosColaboradores[i].Matricula.ToString(),
                    new_clsNovosColaboradores[i].Colaborador,
                    new_clsNovosColaboradores[i].Operadora,
                    new_clsNovosColaboradores[i].Situacao,
                    new_clsNovosColaboradores[i].RG,
                    new_clsNovosColaboradores[i].CPF,
                    new_clsNovosColaboradores[i].Endereco_Residencial,
                    new_clsNovosColaboradores[i].Sic);
            }
        }
        
        private void frmDiferenca_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnGerarExcel_Click(object sender, EventArgs e)
        {
            GerouExcel = true;

            string caminhoSaida = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\Conversor\\BTCC_Inconsistências.xlsx";
            try
            {
                FileInfo newFile = new FileInfo(caminhoSaida);
                ExcelPackage pck = new ExcelPackage(newFile);

                var wInconsistencias = pck.Workbook.Worksheets.Add("Inconsistências");
                wInconsistencias.View.ShowGridLines = true;

                wInconsistencias.Cells["A1:H1"].Merge = true;
                using (var definicoes = wInconsistencias.Cells["A1:H2"])
                {
                    definicoes.Style.Font.Bold = true;
                    definicoes.Style.WrapText = true;
                    definicoes.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    definicoes.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    definicoes.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    definicoes.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(237, 237, 237));
                    definicoes.Style.Font.Size = 12;

                    definicoes.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    definicoes.Style.Border.Bottom.Color.SetColor(Color.Black);
                    definicoes.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    definicoes.Style.Border.Top.Color.SetColor(Color.Black);
                    definicoes.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    definicoes.Style.Border.Left.Color.SetColor(Color.Black);
                    definicoes.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    definicoes.Style.Border.Right.Color.SetColor(Color.Black);
                }
                wInconsistencias.Cells[1, 1, 1, 8].Value = "INCONSISTÊNCIAS";
                wInconsistencias.Cells[2, 1].Value = "Matricula";
                wInconsistencias.Cells[2, 2].Value = "Colaborador";
                wInconsistencias.Cells[2, 3].Value = "Operadora";
                wInconsistencias.Cells[2, 4].Value = "Situação";
                wInconsistencias.Cells[2, 5].Value = "RG";
                wInconsistencias.Cells[2, 6].Value = "CPF";
                wInconsistencias.Cells[2, 7].Value = "Endereço";
                wInconsistencias.Cells[2, 8].Value = "Sic";

                int u = 2;
                int i, j;

                for (i = 0; i < bunifuCustomDataGrid1.RowCount; i++)
                {
                    for (j = 0; j < bunifuCustomDataGrid1.ColumnCount; j++)
                    {
                        wInconsistencias.Cells[u + 1, j + 1].Value = bunifuCustomDataGrid1[j, i].Value.ToString();
                    }
                    u++;
                }
                wInconsistencias.Cells.AutoFitColumns();

                wInconsistencias.Workbook.Properties.Title = "Planilha de Inconsistências";
                wInconsistencias.Workbook.Properties.Author = string.Join(Environment.NewLine, "testeteste@gmail.com", "Guilherme Veiga");
                wInconsistencias.Workbook.Properties.Company = "Tech. Mex";

                pck.Save();

                frmMessage fMessage = new frmMessage("[EXCEL] - Criado com sucesso ! ");
                fMessage.Show();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
            }
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            if(GerouExcel == false && bunifuCustomDataGrid1.RowCount != 0)
            {
                MessageBox.Show("Você não gerou a planilha de inconsistências");
                return;
            }
            this.Close();
        }
    }
}