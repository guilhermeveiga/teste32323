﻿using Conversor.Manager;
using Global;
using System;
using System.Windows.Forms;

namespace Conversor
{
    public partial class Form1 : SombraAereo
    {
        private string sArquivoMesAtual;
        private string sArquivoMesAnterior;

        ManagerArquivo mngArquivo = new ManagerArquivo();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(sArquivoMesAtual) == true && string.IsNullOrEmpty(sArquivoMesAnterior) == true) {
                return;
            }

            string empresa = siG_ComboBox1.Text;
            if (empresa == "BTCC")
            {
                mngArquivo.BTCC_Conversao(empresa, sArquivoMesAtual, sArquivoMesAnterior);
            }
        }

        private void lblArquivoMesAnterior_Click(object sender, EventArgs e)
        {
            string empresa = siG_ComboBox1.Text;

            OpenFileDialog ofd1 = new OpenFileDialog();

            ofd1.Multiselect = true;
            ofd1.Title = "Selecione um Arquivo";
            ofd1.InitialDirectory = @"C:\";
            ofd1.Filter = "Texto (*.txt)|*.txt";
            ofd1.CheckFileExists = true;
            ofd1.CheckPathExists = true;
            ofd1.FilterIndex = 2;
            ofd1.RestoreDirectory = true;
            ofd1.ReadOnlyChecked = true;
            ofd1.ShowReadOnly = true;

            if (ofd1.ShowDialog() == DialogResult.OK)
            {
                sArquivoMesAnterior = ofd1.FileName.ToString();
            }
        }

        private void lblArquivoMesAtual_Click(object sender, EventArgs e)
        {
            string empresa = siG_ComboBox1.Text;

            OpenFileDialog ofd1 = new OpenFileDialog();

            ofd1.Multiselect = true;
            ofd1.Title = "Selecione um Arquivo";
            ofd1.InitialDirectory = @"C:\";
            ofd1.Filter = "Texto (*.txt)|*.txt";
            ofd1.CheckFileExists = true;
            ofd1.CheckPathExists = true;
            ofd1.FilterIndex = 2;
            ofd1.RestoreDirectory = true;
            ofd1.ReadOnlyChecked = true;
            ofd1.ShowReadOnly = true;

            if (ofd1.ShowDialog() == DialogResult.OK)
            {
                sArquivoMesAtual = ofd1.FileName.ToString();
            }
        }

        private void lblCNPJ_Click(object sender, EventArgs e)
        {

        }

        private void siG_ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string empresa = siG_ComboBox1.Text;
            switch (empresa)
            {
                case "BTCC":
                    {
                        lblCNPJ.Text = "23.258.369/0001-35";
                        lblNomeFantasia.Text = "BTCC";
                        lblRazaoSocial.Text = "BTCC NÃO SEI";
                    }
                    break;
                default:
                    break;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
