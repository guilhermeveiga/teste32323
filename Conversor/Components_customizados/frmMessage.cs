﻿using Global;
using System;

namespace Conversor.Components_customizados
{
    public partial class frmMessage : SombraAereo
    {
        public frmMessage(string valorEncontrado, int iQuantidadeNovosColaboradores)
        {
            InitializeComponent();
            lblValorEncontrando.Text = valorEncontrado;
            lblNovosColaboradores.Text = iQuantidadeNovosColaboradores.ToString();
        }

        public frmMessage(string texto)
        {
            InitializeComponent();
            label12.Text = texto;
            label11.Visible = false;
            label13.Visible = false;
            lblValorEncontrando.Visible = false;
            lblNovosColaboradores.Visible = false;
        }

        private void frmMessage_Load(object sender, EventArgs e)
        {

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
