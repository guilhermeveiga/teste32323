﻿namespace Conversor.Components_customizados
{
    partial class frmMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMessage));
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.icon = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblNovosColaboradores = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblValorEncontrando = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.icon)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 20;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.White;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "OK";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleBorderThickness = 1;
            this.bunifuThinButton21.IdleCornerRadius = 20;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Location = new System.Drawing.Point(16, 358);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(419, 41);
            this.bunifuThinButton21.TabIndex = 0;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // icon
            // 
            this.icon.BackColor = System.Drawing.Color.Transparent;
            this.icon.Enabled = false;
            this.icon.Image = ((System.Drawing.Image)(resources.GetObject("icon.Image")));
            this.icon.Location = new System.Drawing.Point(31, -21);
            this.icon.Name = "icon";
            this.icon.Size = new System.Drawing.Size(372, 270);
            this.icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon.TabIndex = 4;
            this.icon.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label12.ForeColor = System.Drawing.Color.DimGray;
            this.label12.Location = new System.Drawing.Point(90, 204);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(237, 21);
            this.label12.TabIndex = 23;
            this.label12.Text = "RESULTADO DO ARQUIVO VIGENTE";
            // 
            // lblNovosColaboradores
            // 
            this.lblNovosColaboradores.AutoSize = true;
            this.lblNovosColaboradores.BackColor = System.Drawing.Color.Transparent;
            this.lblNovosColaboradores.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblNovosColaboradores.ForeColor = System.Drawing.Color.DimGray;
            this.lblNovosColaboradores.Location = new System.Drawing.Point(209, 298);
            this.lblNovosColaboradores.Name = "lblNovosColaboradores";
            this.lblNovosColaboradores.Size = new System.Drawing.Size(28, 21);
            this.lblNovosColaboradores.TabIndex = 26;
            this.lblNovosColaboradores.Text = "N/I";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label13.ForeColor = System.Drawing.Color.DimGray;
            this.label13.Location = new System.Drawing.Point(18, 298);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(185, 21);
            this.label13.TabIndex = 25;
            this.label13.Text = "COLABORADORES NOVOS:";
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.bunifuSeparator2.ForeColor = System.Drawing.Color.DimGray;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator2.LineThickness = 2;
            this.bunifuSeparator2.Location = new System.Drawing.Point(39, 229);
            this.bunifuSeparator2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(355, 14);
            this.bunifuSeparator2.TabIndex = 24;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // lblValorEncontrando
            // 
            this.lblValorEncontrando.AutoSize = true;
            this.lblValorEncontrando.BackColor = System.Drawing.Color.Transparent;
            this.lblValorEncontrando.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.lblValorEncontrando.ForeColor = System.Drawing.Color.DimGray;
            this.lblValorEncontrando.Location = new System.Drawing.Point(209, 263);
            this.lblValorEncontrando.Name = "lblValorEncontrando";
            this.lblValorEncontrando.Size = new System.Drawing.Size(28, 21);
            this.lblValorEncontrando.TabIndex = 22;
            this.lblValorEncontrando.Text = "N/I";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Montserrat", 9.749999F);
            this.label11.ForeColor = System.Drawing.Color.DimGray;
            this.label11.Location = new System.Drawing.Point(49, 263);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 21);
            this.label11.TabIndex = 21;
            this.label11.Text = "VALOR ENCONTRADO:";
            // 
            // frmMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 421);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblNovosColaboradores);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.lblValorEncontrando);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.icon);
            this.Controls.Add(this.bunifuThinButton21);
            this.Name = "frmMessage";
            this.Text = "frmMessage";
            this.Load += new System.EventHandler(this.frmMessage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.icon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private System.Windows.Forms.PictureBox icon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblNovosColaboradores;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private System.Windows.Forms.Label lblValorEncontrando;
        private System.Windows.Forms.Label label11;
    }
}