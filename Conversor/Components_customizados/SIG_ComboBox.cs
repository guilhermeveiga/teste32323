﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Conversor.Components_customizados
{
    public partial class SIG_ComboBox : ComboBox
    {
        [DefaultValue(DrawMode.OwnerDrawFixed)]
        [Browsable(false)]
        public new DrawMode DrawMode
        {
            get { return DrawMode.OwnerDrawFixed; }
            set { base.DrawMode = DrawMode.OwnerDrawFixed; }
        }

        [DefaultValue(ComboBoxStyle.DropDownList)]
        [Browsable(false)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return ComboBoxStyle.DropDownList; }
            set { base.DropDownStyle = ComboBoxStyle.DropDownList; }
        }

        private string promptText = "";
        [Browsable(true)]
        [DefaultValue("")]
        [Category("teste")]
        public string PromptText
        {
            get { return promptText; }
            set { promptText = value ?? String.Empty; Invalidate(); }
        }

        private bool drawPrompt;

        Font COMFORTAA_10 = new Font("Comfortaa", 9, FontStyle.Regular);

        private void UseSelectable()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.Selectable, true);
            //ButtonState = new Helper.ButtonState(this);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public SIG_ComboBox()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);

            UseSelectable();

            base.DrawMode = DrawMode.OwnerDrawFixed;
            base.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            int buttonWidth = SystemInformation.HorizontalScrollBarArrowWidth;
            var brush2 = new SolidBrush(Color.Blue);
            base.OnPaintBackground(e);
            using (var brush = new SolidBrush(BackColor))
            {

                e.Graphics.FillRectangle(brush, ClientRectangle);
                //e.Graphics.DrawLine(Pens.Black, 10, 10, ClientSize.Width - 1, ClientSize.Height - 1);

                //e.Graphics.DrawRectangle(Pens.DarkGray, 0, 0, ClientSize.Width - 1, ClientSize.Height - 1);
            }
        }


        Color foreColor = Color.Black;
        Color backColor = Color.Transparent;

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.LightGray, 0, this.ClientSize.Height - 1, this.ClientSize.Width, this.ClientSize.Height - 1); // desenha linha de baixo

            // drop-down arrow
            using (SolidBrush b = new SolidBrush(foreColor))
            {
                e.Graphics.FillPolygon(b, new[]
                    {
                        new Point(Width - 20, (Height / 2) - 2),
                        new Point(Width - 9, (Height / 2) - 2),
                        new Point(Width - 15,  (Height / 2) + 4)
                    });
            }
            Rectangle textRect = new Rectangle(2, 2, Width - 20, Height - 4);
            TextRenderer.DrawText(e.Graphics, Text, COMFORTAA_10, textRect, foreColor, /*EffectiveBackColor,*/ TextFormatFlags.Left | TextFormatFlags.VerticalCenter);

            if (drawPrompt)
            {
                DrawTextPrompt(e.Graphics);
            }
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (e.Index < 0)
            {
                base.OnDrawItem(e);
                return;
            }

            bool normal = e.State == (DrawItemState.NoAccelerator | DrawItemState.NoFocusRect) || e.State == DrawItemState.None;
            //// TODO: We need a real background color for transparent drawing
            //Color backColor = normal ? EffectiveBackColor : GetStyleColor();
            //// replace transparent background colors for drawing items
            //if (normal && backColor.A < 255) backColor = GetThemeColor("BackColor");
            //// TODO: We need complementing foreground colors for the styles !!
            //Color foreColor = normal ? EffectiveForeColor : GetThemeColor("Tile.ForeColor.Normal");

            e.Graphics.DrawLine(Pens.Gainsboro, new Point(e.Bounds.Left, e.Bounds.Bottom - 1),
                new Point(e.Bounds.Right, e.Bounds.Bottom - 1));

            //risco no meio da letra
            //var ordinate = e.Bounds.Top + e.Bounds.Height / 2;
            //e.Graphics.DrawLine(new Pen(Color.Gainsboro), new Point(e.Bounds.Left, ordinate),
            //    new Point(e.Bounds.Right, ordinate));

            //Sem Divisão
            //using (SolidBrush b = new SolidBrush(backColor))
            //{
            //    e.Graphics.FillRectangle(b, e.Bounds);
            //}

            TextRenderer.DrawText(e.Graphics, GetItemText(Items[e.Index]), COMFORTAA_10, e.Bounds, foreColor, backColor, TextFormatFlags.Left | TextFormatFlags.VerticalCenter);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            ItemHeight = GetPreferredSize(Size.Empty).Height;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Width = Width;

            Height = this.Height + 5;
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            // AFAIK, OCM_MESSAGES is MFC-specific. The WM_PAINT is already handled by us.
            //const int OCM_COMMAND = WinApi.Messages.OCM__BASE + WinApi.Messages.WM_COMMAND;
            //if (((m.Msg == WinApi.Messages.WM_PAINT) || (m.Msg == OCM_COMMAND)) && (drawPrompt))
            //{
            //DrawTextPrompt();
            //}
        }

        private void DrawTextPrompt()
        {
            using (Graphics graphics = CreateGraphics())
            {
                DrawTextPrompt(graphics);
            }
        }

        private void DrawTextPrompt(Graphics g)
        {
            Rectangle textRect = new Rectangle(2, 2, Width - 20, Height - 4);
            TextRenderer.DrawText(g, promptText, COMFORTAA_10, textRect, foreColor,
               /*EffectiveBackColor,*/ TextFormatFlags.Left | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);
        }

        public override Size GetPreferredSize(Size proposedSize)
        {
            using (var g = CreateGraphics())
            {
                string measureText = Text.Length > 0 ? Text : "testaaaa";
                proposedSize = new Size(int.MaxValue, int.MaxValue);
                Size preferredSize = TextRenderer.MeasureText(g, measureText, COMFORTAA_10, proposedSize, TextFormatFlags.LeftAndRightPadding);
                preferredSize.Height += 4;
                return preferredSize;
            }
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
            drawPrompt = (SelectedIndex == -1);
            Invalidate();
        }
    }
}
