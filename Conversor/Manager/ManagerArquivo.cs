﻿using Conversor.Components_customizados;
using Global;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Conversor.Manager
{
    public class ManagerArquivo
    {
        public List<clsUnidade> lstUnidade = new List<clsUnidade>();
        public List<clsDepartamento> lstDepartamento = new List<clsDepartamento>();
        public List<clsFuncionario> lstFuncionario = new List<clsFuncionario>();
        public List<clsPedido> lstPedido = new List<clsPedido>();
        public List<clsHeader> lstHeader = new List<clsHeader>();
        public List<clsOperadoras> lstOperadoras = new List<clsOperadoras>();
        //verificação do novo arquivo
        public List<clsNovosColaboradores> lstNovosColaboradores = new List<clsNovosColaboradores>();
        //
        public List<clsColaboradoresAntigos> lstColaboradoresAntigos = new List<clsColaboradoresAntigos>();
        public List<clsPedidoAntigos> lstPedidoAntigos = new List<clsPedidoAntigos>();

        public void BTCC_Conversao(string NomeEmpresaSelecionada, string CaminhoArquivoMesAtual, string CaminhoArquivoMesAnterior)
        {
            var tempo = System.Diagnostics.Stopwatch.StartNew(); // inicia o contador de tempo            
            clsHeader header = new clsHeader();

            if (Directory.Exists(globalDefs.caminhoSaida) == false)
            {
                Directory.CreateDirectory(globalDefs.caminhoSaida);
            }
            globalDefs.caminhoSaida = globalDefs.caminhoSaida + "BTCC_Dados.xlsx";

            string[] lines = System.IO.File.ReadAllLines(CaminhoArquivoMesAtual);

            try
            {
                foreach (string line in lines)
                {
                    string IndexBloco = line.Substring(0, 4);
                    try
                    {
                        switch (IndexBloco)
                        {
                            case "TTPE":
                                {
                                    header.ValorArquivo = line.Substring(72, 11).TrimStart(new Char[] { '0' });
                                    lstHeader.Add(header);
                                }
                                break;
                            case "TTUN":
                                {
                                    clsUnidade unidade = new clsUnidade();
                                    unidade.CNPJ = line.Substring(4, 18);
                                    unidade.CodUnidade = line.Substring(32, 6);
                                    unidade.Endereco = line.Substring(68, 30);
                                    unidade.Bairro = line.Substring(114, 15);
                                    unidade.Cidade = line.Substring(129, 25);
                                    unidade.Cep = line.Substring(154, 9);
                                    unidade.UF = line.Substring(163, 2);
                                    lstUnidade.Add(unidade);
                                }
                                break;
                            case "TTDE":
                                {
                                    clsDepartamento departamento = new clsDepartamento();
                                    departamento.CNPJ = line.Substring(4, 18);
                                    departamento.CodUnidade = line.Substring(32, 6);
                                    departamento.CodDepartamento = line.Substring(38, 6);
                                    departamento.Departamento = line.Substring(44, 46);
                                    lstDepartamento.Add(departamento);
                                }
                                break;
                            case "TTFU":
                                {
                                    clsFuncionario funcionario = new clsFuncionario();
                                    funcionario.CNPJ = line.Substring(4, 18);
                                    funcionario.Empresa = NomeEmpresaSelecionada;
                                    funcionario.CodUnidade = line.Substring(32, 6);
                                    funcionario.CodDepartamento = line.Substring(38, 6);
                                    funcionario.Departamento = line.Substring(44, 46);
                                    funcionario.ID = line.Substring(303, 11);
                                    funcionario.Matricula = line.Substring(44, 12).TrimStart(new Char[] { '0' });
                                    funcionario.Funcionario = line.Substring(56, 30);
                                    funcionario.RG = line.Substring(86, 10);
                                    funcionario.CPF = line.Substring(96, 11);
                                    funcionario.Data_Nascimento = line.Substring(111, 8);
                                    funcionario.Sexo = line.Substring(121, 1);
                                    funcionario.Nome_Mae = line.Substring(122, 35);
                                    funcionario.Endereco_Residencial = line.Substring(157, 40);
                                    funcionario.Municipio_Residencial = line.Substring(218, 40);
                                    funcionario.Bairro_Residencial = line.Substring(258, 30);
                                    funcionario.Cep_Residencial = line.Substring(288, 8);
                                    lstFuncionario.Add(funcionario);
                                }
                                break;
                            case "TTIT":
                                {
                                    clsPedido pedido = new clsPedido();
                                    pedido.CNPJ = line.Substring(4, 18);
                                    pedido.Unidade = line.Substring(32, 6).TrimStart(new Char[] { '0' });
                                    pedido.Departamento = line.Substring(38, 6).TrimStart(new Char[] { '0' });
                                    pedido.Matricula = line.Substring(44, 12).TrimStart(new Char[] { '0' });
                                    //GAP - 56 ao 59
                                    pedido.Qtd = line.Substring(59, 8).TrimStart(new Char[] { '0' });
                                    pedido.Valor_Facial = line.Substring(67, 9).TrimStart(new Char[] { '0' }).Replace(".", ",");
                                    pedido.Operadora = line.Substring(76, 22).TrimStart(new Char[] { '0' });
                                    pedido.Total = (Convert.ToDecimal(pedido.Qtd) * Convert.ToDecimal(pedido.Valor_Facial));
                                    lstPedido.Add(pedido);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception erro)
                    {
                        continue;
                    }
                }
            }
            catch (Exception erro)
            {

            }

            carregaConcatenacaoUnidade();
            verificaPedidos();
            criaExcel();
            BTCC_Comparativo(CaminhoArquivoMesAnterior);

            //tempo.Stop();
            //var elapsedMs = tempo.Elapsed.TotalMinutes;            
        }

        public void BTCC_Comparativo(string caminhoArquivoMesAnterior)
        {
            string[] lines = System.IO.File.ReadAllLines(caminhoArquivoMesAnterior);
            int iNovosColaboradores = 0;

            // preenche a planilha do mês anterior
            try
            {
                foreach (string line in lines)
                {
                    string IndexBloco = line.Substring(0, 4);

                    if (IndexBloco == "TTFU")
                    {
                        clsColaboradoresAntigos cColaboradoresAntigos = new clsColaboradoresAntigos();
                        cColaboradoresAntigos.Matricula = line.Substring(44, 12).TrimStart(new Char[] { '0' });
                        cColaboradoresAntigos.Colaborador = line.Substring(56, 30);
                        cColaboradoresAntigos.RG = line.Substring(86, 10);
                        cColaboradoresAntigos.CPF = line.Substring(96, 11);
                        cColaboradoresAntigos.Data_Nascimento = line.Substring(111, 8);
                        cColaboradoresAntigos.Endereco_Residencial = line.Substring(157, 40);

                        lstColaboradoresAntigos.Add(cColaboradoresAntigos);
                    }
                    else if (IndexBloco == "TTIT")
                    {
                        clsPedidoAntigos pedidoAntigos = new clsPedidoAntigos();
                        pedidoAntigos.Matricula = line.Substring(44, 12).TrimStart(new Char[] { '0' });
                        pedidoAntigos.Operadora = line.Substring(76, 22).TrimStart(new Char[] { '0' });
                        lstPedidoAntigos.Add(pedidoAntigos);
                    }
                }
                verificaPedidos_comparacao();
                frmMessage fMessage = new frmMessage("R$ " + lstHeader[0].ValorArquivo, iNovosColaboradores);
                //fMessage.Show();
            }
            catch (Exception erro)
            {

            }

            int iCountAntigo = lstColaboradoresAntigos.Count;
            int iCountNovo = lstFuncionario.Count;


            int diferenca = iCountNovo - iCountAntigo;

            if (diferenca < 0)
            {
                for (int i = 0; i < iCountAntigo; i++)
                {
                    string matriculaOld = lstColaboradoresAntigos[i].Matricula; // procurar esse infeliz em toda a lista anterior
                    string colaborador = lstColaboradoresAntigos[i].Colaborador;
                    string operadora = lstColaboradoresAntigos[i].Op1;
                    string rg = lstColaboradoresAntigos[i].RG;
                    string cpf = lstColaboradoresAntigos[i].CPF;

                    if (!lstFuncionario.Exists(x => x.Matricula == matriculaOld))
                    {
                        clsNovosColaboradores cNovosColaboradores = new clsNovosColaboradores();
                        cNovosColaboradores.Matricula = matriculaOld;
                        cNovosColaboradores.Colaborador = colaborador;
                        cNovosColaboradores.Operadora = operadora;
                        cNovosColaboradores.Situacao = "DESLIGADO | FÉRIAS";
                        cNovosColaboradores.RG = rg;
                        cNovosColaboradores.CPF = cpf;

                        lstNovosColaboradores.Add(cNovosColaboradores);
                        //MessageBox.Show("mat: " + matriculaOld + " Desligado \n Férias");
                    }
                }
            }
            else
            {
                for (int i = 0; i < iCountNovo; i++)
                {
                    string matriculaOld = lstFuncionario[i].Matricula; // procurar esse infeliz em toda a lista anterior
                    string colaborador = lstFuncionario[i].Funcionario;
                    string operadora = lstFuncionario[i].Op1;
                    string rg = lstFuncionario[i].RG;
                    string cpf = lstFuncionario[i].CPF;

                    if (!lstColaboradoresAntigos.Exists(x => x.Matricula == matriculaOld))
                    {
                        clsNovosColaboradores cNovosColaboradores = new clsNovosColaboradores();
                        cNovosColaboradores.Matricula = matriculaOld;
                        cNovosColaboradores.Colaborador = colaborador;
                        cNovosColaboradores.Operadora = operadora;
                        cNovosColaboradores.Situacao = "NOVO | FÉRIAS";
                        cNovosColaboradores.RG = rg;
                        cNovosColaboradores.CPF = cpf;

                        lstNovosColaboradores.Add(cNovosColaboradores);
                        //MessageBox.Show("mat: " + matriculaOld + " Não Contem no antigo mas \n contem no novo");
                    }
                }
            }
            frmDiferenca f = new frmDiferenca(lstNovosColaboradores, true);
            f.Show();
        }
                
        public void verificaPedidos()
        {
            decimal q1;
            decimal v1;
            decimal s1;

            int i;
            Console.WriteLine("[" + DateTime.Now + "][TXT] - Verificando pedidos...");
            for (i = 0; i < lstFuncionario.Count; i++)
            {
                for (int x = 0; x < lstPedido.Count; x++)
                {
                    //Console.WriteLine("lstFuncionario: " + lstFuncionario[i].Matricula + "  lstPedido: " + lstPedido[x].Matricula);
                    if (lstFuncionario[i].Matricula == lstPedido[x].Matricula)
                    {
                        if (lstFuncionario[i].Qvt1 == null || lstFuncionario[i].Qvt1.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op1] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvl1] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op1 = lstPedido[x].Operadora.Trim();
                            lstFuncionario[i].Qvt1 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt1 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt1);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt1);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma1 = s1;
                        }
                        else if (lstFuncionario[i].Qvt2 == null || lstFuncionario[i].Qvt2.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op2] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvl2] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op2 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt2 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt2 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt2);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt2);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma2 = s1;
                        }
                        else if (lstFuncionario[i].Qvt3 == null || lstFuncionario[i].Qvt3.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op3] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvl3] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op3 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt3 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt3 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt3);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt3);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma3 = s1;
                        }
                        else if (lstFuncionario[i].Qvt4 == null || lstFuncionario[i].Qvt4.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op4] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvl4] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op4 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt4 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt4 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt4);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt4);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma4 = s1;
                        }
                        else if (lstFuncionario[i].Qvt5 == null || lstFuncionario[i].Qvt5.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op5] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvl5] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op5 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt5 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt5 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt5);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt5);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma5 = s1;
                        }
                        else if (lstFuncionario[i].Qvt6 == null || lstFuncionario[i].Qvt6.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op6] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvt6] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op6 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt6 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt6 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt6);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt6);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma6 = s1;
                        }
                        else if (lstFuncionario[i].Qvt7 == null || lstFuncionario[i].Qvt7.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op6] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvt6] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op7 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt7 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt7 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt7);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt7);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma7 = s1;
                        }
                        else if (lstFuncionario[i].Qvt8 == null || lstFuncionario[i].Qvt8.Length < 0)
                        {
                            //Console.WriteLine("Pedido : [Op6] - [Matricula] : " + lstPedido[x].Matricula + " -  [Qvt6] : " + lstPedido[x].Valor_Facial + " -  [Opr] : " + lstPedido[x].Operadora);
                            lstFuncionario[i].Op8 = lstPedido[x].Operadora;
                            lstFuncionario[i].Qvt8 = lstPedido[x].Qtd;
                            lstFuncionario[i].Vvt8 = lstPedido[x].Valor_Facial;

                            q1 = Convert.ToDecimal(lstFuncionario[i].Qvt8);
                            v1 = Convert.ToDecimal(lstFuncionario[i].Vvt8);
                            s1 = (q1 * v1);

                            lstFuncionario[i].soma8 = s1;
                        }
                        else
                        {
                            MessageBox.Show("Erro > 8");
                        }

                        decimal soma = lstFuncionario[i].soma1 +
                            lstFuncionario[i].soma2 +
                            lstFuncionario[i].soma3 +
                            lstFuncionario[i].soma4 +
                            lstFuncionario[i].soma5 +
                            lstFuncionario[i].soma6 +
                            lstFuncionario[i].soma7 +
                            lstFuncionario[i].soma8;

                        lstFuncionario[i].somaGeral = soma;
                    }
                }
            }
        }

        public void carregaConcatenacaoUnidade()
        {
            for (int x = 0; x < lstFuncionario.Count; x++)
            {
                try
                {
                    string CodigoUnidade_Funcionario = lstFuncionario[x].CodUnidade;
                    string CodigoDepartamento_Funcionario = lstFuncionario[x].CodDepartamento;

                    for (int i = 0; i < lstUnidade.Count; i++)
                    {
                        try
                        {
                            string CodigoUnidade_Unidade = lstUnidade[i].CodUnidade;

                            if (CodigoUnidade_Funcionario.ToString() == CodigoUnidade_Unidade.ToString())
                            {
                                lstFuncionario[x].Endereco = lstUnidade[i].Endereco; // Endereço
                                lstFuncionario[x].Bairro = lstUnidade[i].Bairro; // bairro
                                lstFuncionario[x].Cidade = lstUnidade[i].Cidade; // Cidade
                                lstFuncionario[x].UF = lstUnidade[i].UF; // UF
                                lstFuncionario[x].Cep = lstUnidade[i].Cep; // Cep
                            }
                        }
                        catch (Exception erro)
                        {
                            continue;
                        }
                    }
                    for (int i = 0; i < lstDepartamento.Count; i++)
                    {
                        try
                        {
                            string CodigoDepartamento_Departamento = lstDepartamento[i].CodDepartamento;

                            if (CodigoDepartamento_Funcionario.ToString() == CodigoDepartamento_Departamento.ToString())
                            {
                                lstFuncionario[x].Departamento = lstDepartamento[i].Departamento;
                            }
                        }
                        catch (Exception erro)
                        {
                            continue;
                        }
                    }
                }
                catch (Exception erro)
                {
                    continue;
                }
            }
        }
        
        public void verificaPedidos_comparacao()
        {
            int i;
            Console.WriteLine("[" + DateTime.Now + "][TXT] - Verificando pedidos...");
            for (i = 0; i < lstColaboradoresAntigos.Count; i++)
            {
                for (int x = 0; x < lstPedidoAntigos.Count; x++)
                {
                    if (lstColaboradoresAntigos[i].Matricula == lstPedidoAntigos[x].Matricula)
                    {
                        if (lstColaboradoresAntigos[i].Qvt1 == null || lstColaboradoresAntigos[i].Qvt1.Length < 0)
                        {
                            lstColaboradoresAntigos[i].Op1 = lstPedidoAntigos[x].Operadora.Trim();
                        }
                        //else if (lstColaboradoresAntigos[i].Qvt2 == null || lstColaboradoresAntigos[i].Qvt2.Length < 0)
                        //{
                        //    lstColaboradoresAntigos[i].Op2 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstColaboradoresAntigos[i].Qvt3 == null || lstColaboradoresAntigos[i].Qvt3.Length < 0)
                        //{
                        //    lstColaboradoresAntigos[i].Op3 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstColaboradoresAntigos[i].Qvt4 == null || lstColaboradoresAntigos[i].Qvt4.Length < 0)
                        //{
                        //    lstColaboradoresAntigos[i].Op4 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstColaboradoresAntigos[i].Qvt5 == null || lstColaboradoresAntigos[i].Qvt5.Length < 0)
                        //{
                        //    lstColaboradoresAntigos[i].Op5 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstFuncionario[i].Qvt6 == null || lstFuncionario[i].Qvt6.Length < 0)
                        //{
                        //    lstFuncionario[i].Op6 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstFuncionario[i].Qvt7 == null || lstFuncionario[i].Qvt7.Length < 0)
                        //{
                        //    lstFuncionario[i].Op7 = lstPedidoAntigos[x].Operadora;
                        //}
                        //else if (lstFuncionario[i].Qvt8 == null || lstFuncionario[i].Qvt8.Length < 0)
                        //{
                        //    lstFuncionario[i].Op8 = lstPedidoAntigos[x].Operadora;
                        //}
                        else
                        {
                            MessageBox.Show("Erro > 8");
                        }
                    }
                }
            }
        }

        public void criaExcel()
        {
            //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando...");
            try
            {
                FileInfo newFile = new FileInfo(globalDefs.caminhoSaida);
                ExcelPackage pck = new ExcelPackage(newFile);

                /* Unidade */
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando Unidades...");
                var wUnidade = pck.Workbook.Worksheets.Add("Unidade");
                wUnidade.View.ShowGridLines = true;


                wUnidade.Cells[1, 1].Value = "Server = 1";
                wUnidade.Cells[1, 2].Value = "CNPJ";
                wUnidade.Cells[1, 3].Value = "CodUnidade";
                wUnidade.Cells[1, 4].Value = "Endereco";
                wUnidade.Cells[1, 5].Value = "Bairro";
                wUnidade.Cells[1, 6].Value = "Cidade";
                wUnidade.Cells[1, 7].Value = "UF";
                wUnidade.Cells[1, 8].Value = "Cep";
                wUnidade.Cells[1, 1, 1, 8].Style.Font.Bold = true;
                wUnidade.Cells[1, 1, 1, 8].Style.Font.Color.SetColor(Color.Red);


                int UnidadeCount = lstUnidade.Count;
                int q = 2;

                for (int i = 0; i < UnidadeCount; i++)
                {  //Console.WriteLine("[" + i + "] de [" + UnidadeCount + "] - Unidade");
                    wUnidade.Cells[q, 1].Value = lstUnidade[i].CNPJ + " - " + lstUnidade[i].CodUnidade; //  RemoverAcentos(lstUnidade[i].CNPJ + " - " + lstUnidade[i].CodUnidade);               // "Server = 1";
                    wUnidade.Cells[q, 2].Value = lstUnidade[i].CNPJ; // RemoverAcentos(lstUnidade[i].CNPJ);                 //  "CNPJ";
                    wUnidade.Cells[q, 3].Value = lstUnidade[i].CodUnidade; // RemoverAcentos(lstUnidade[i].CodUnidade);              //  "Enoresa";
                    wUnidade.Cells[q, 4].Value = lstUnidade[i].Endereco; // RemoverAcentos(lstUnidade[i].Endereco);           //  "C.Unid";
                    string bairro = lstUnidade[i].Bairro; // RemoverAcentos(lstUnidade[i].Bairro);
                    wUnidade.Cells[q, 5].Value = bairro;      //  "C.Depto";
                    wUnidade.Cells[q, 6].Value = lstUnidade[i].Cidade; // RemoverAcentos(lstUnidade[i].Cidade);         //  "Depto";
                    wUnidade.Cells[q, 7].Value = lstUnidade[i].UF; // RemoverAcentos(lstUnidade[i].UF);               //  "Bairro";
                    wUnidade.Cells[q, 8].Value = lstUnidade[i].Cep; // RemoverAcentos(lstUnidade[i].Cep);             //  "Endereço";
                    q++;
                }
                wUnidade.Cells.AutoFitColumns();

                /* Departamento */
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando Departamentos...");
                var wDepartamento = pck.Workbook.Worksheets.Add("Departamento");
                wDepartamento.View.ShowGridLines = true;

                wDepartamento.Cells[1, 1].Value = "Server = 1";
                wDepartamento.Cells[1, 2].Value = "CNPJ";
                wDepartamento.Cells[1, 3].Value = "CodUnidade";
                wDepartamento.Cells[1, 4].Value = "CodDepartamento";
                wDepartamento.Cells[1, 5].Value = "Departamento";

                wDepartamento.Cells[1, 1, 1, 5].Style.Font.Bold = true;

                int DepartamentoCount = lstDepartamento.Count;
                int w = 2;
                for (int i = 0; i < DepartamentoCount; i++)
                {   //Console.WriteLine("[" + i + "] de [" + DepartamentoCount + "] - Departamento");
                    wDepartamento.Cells[w, 1].Value = lstDepartamento[i].CNPJ + " - " + lstDepartamento[i].CodUnidade + " - " + lstDepartamento[i].CodDepartamento;               // "Server = 1";
                    wDepartamento.Cells[w, 2].Value = lstDepartamento[i].CNPJ;
                    wDepartamento.Cells[w, 3].Value = lstDepartamento[i].CodUnidade;
                    wDepartamento.Cells[w, 4].Value = lstDepartamento[i].CodDepartamento;
                    wDepartamento.Cells[w, 5].Value = lstDepartamento[i].Departamento;
                    w++;
                }
                wDepartamento.Cells.AutoFitColumns();

                /* Funcionario */
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando Funcionarios...");
                var ws = pck.Workbook.Worksheets.Add("Funcionario");
                ws.View.ShowGridLines = true;

                ws.Cells[1, 1].Value = "Server = 1";
                ws.Cells[1, 2].Value = "CNPJ";
                ws.Cells[1, 3].Value = "Empresa";
                ws.Cells[1, 4].Value = "C.Unid";
                ws.Cells[1, 5].Value = "C.Depto";
                ws.Cells[1, 6].Value = "Depto";
                ws.Cells[1, 7].Value = "Endereço";
                ws.Cells[1, 8].Value = "Bairro";
                ws.Cells[1, 9].Value = "Cidade";
                ws.Cells[1, 10].Value = "UF";
                ws.Cells[1, 11].Value = "Cep";
                ws.Cells[1, 12].Value = "ID";
                ws.Cells[1, 13].Value = "Matrícula";
                ws.Cells[1, 14].Value = "Funcionário";
                ws.Cells[1, 15].Value = "RG";
                ws.Cells[1, 16].Value = "CPF";
                ws.Cells[1, 17].Value = "Data_Nasc";
                ws.Cells[1, 18].Value = "Sexo";
                ws.Cells[1, 19].Value = "Nome_Mãe";
                ws.Cells[1, 20].Value = "Endereço_Res";
                ws.Cells[1, 21].Value = "Bairro_Res";
                ws.Cells[1, 22].Value = "Município_Res";
                ws.Cells[1, 23].Value = "CEP_Res";
                ws.Cells[1, 24].Value = "Op1";
                ws.Cells[1, 25].Value = "Qvt1";
                ws.Cells[1, 26].Value = "Vvt1";
                ws.Cells[1, 27].Value = "Op2";
                ws.Cells[1, 28].Value = "Qvt2";
                ws.Cells[1, 29].Value = "Vvt2";
                ws.Cells[1, 30].Value = "Op3";
                ws.Cells[1, 31].Value = "Qvt3";
                ws.Cells[1, 32].Value = "Vvt3";
                ws.Cells[1, 33].Value = "Op4";
                ws.Cells[1, 34].Value = "Qvt4";
                ws.Cells[1, 35].Value = "Vvt4";
                ws.Cells[1, 36].Value = "Op5";
                ws.Cells[1, 37].Value = "Qvt5";
                ws.Cells[1, 38].Value = "Vvt5";
                ws.Cells[1, 39].Value = "Op6";
                ws.Cells[1, 40].Value = "Qvt6";
                ws.Cells[1, 41].Value = "Vvt6";
                ws.Cells[1, 42].Value = "Soma";

                ws.Cells[1, 1, 1, 41].Style.Font.Bold = true;

                int FuncionarioCount = lstFuncionario.Count;
                int p = 2;
                for (int i = 0; i < FuncionarioCount; i++)
                {   //Console.WriteLine("["+ i + "] de ["+ FuncionarioCount+"] Funcionários");
                    ws.Cells[p, 1].Value = lstFuncionario[i].UF + " - " + lstFuncionario[i].CNPJ;
                    ws.Cells[p, 2].Value = lstFuncionario[i].CNPJ;
                    ws.Cells[p, 3].Value = lstFuncionario[i].Empresa;
                    ws.Cells[p, 4].Value = lstFuncionario[i].CodUnidade;
                    ws.Cells[p, 5].Value = lstFuncionario[i].CodDepartamento;
                    ws.Cells[p, 6].Value = lstFuncionario[i].Departamento;
                    ws.Cells[p, 7].Value = lstFuncionario[i].Endereco;
                    ws.Cells[p, 8].Value = lstFuncionario[i].Bairro;
                    ws.Cells[p, 9].Value = lstFuncionario[i].Cidade;
                    ws.Cells[p, 10].Value = lstFuncionario[i].UF;
                    ws.Cells[p, 11].Value = lstFuncionario[i].Cep;
                    ws.Cells[p, 12].Value = lstFuncionario[i].ID;
                    ws.Cells[p, 13].Value = lstFuncionario[i].Matricula;
                    ws.Cells[p, 14].Value = lstFuncionario[i].Funcionario;
                    ws.Cells[p, 15].Value = lstFuncionario[i].RG;                  //  "RG";
                                                                                   // x.Cells[p, 16] = "=\"" + lstFuncionario[i].CPF + "\"";                 //  "CPF";
                    ws.Cells[p, 16].Value = lstFuncionario[i].CPF;
                    ws.Cells[p, 17].Value = lstFuncionario[i].Data_Nascimento;
                    ws.Cells[p, 18].Value = lstFuncionario[i].Sexo;
                    ws.Cells[p, 19].Value = lstFuncionario[i].Nome_Mae;
                    ws.Cells[p, 20].Value = lstFuncionario[i].Endereco_Residencial;
                    ws.Cells[p, 21].Value = lstFuncionario[i].Bairro_Residencial;
                    ws.Cells[p, 22].Value = lstFuncionario[i].Municipio_Residencial;
                    ws.Cells[p, 23].Value = lstFuncionario[i].Cep_Residencial;
                    ws.Cells[p, 24].Value = lstFuncionario[i].Op1;
                    ws.Cells[p, 25].Value = lstFuncionario[i].Qvt1;
                    ws.Cells[p, 26].Value = lstFuncionario[i].Vvt1;
                    ws.Cells[p, 27].Value = lstFuncionario[i].Op2;
                    ws.Cells[p, 28].Value = lstFuncionario[i].Qvt2;
                    ws.Cells[p, 29].Value = lstFuncionario[i].Vvt2;
                    ws.Cells[p, 30].Value = lstFuncionario[i].Op3;
                    ws.Cells[p, 31].Value = lstFuncionario[i].Qvt3;
                    ws.Cells[p, 32].Value = lstFuncionario[i].Vvt3;
                    ws.Cells[p, 33].Value = lstFuncionario[i].Op4;
                    ws.Cells[p, 34].Value = lstFuncionario[i].Qvt4;
                    ws.Cells[p, 35].Value = lstFuncionario[i].Vvt4;
                    ws.Cells[p, 36].Value = lstFuncionario[i].Op5;
                    ws.Cells[p, 37].Value = lstFuncionario[i].Qvt5;
                    ws.Cells[p, 38].Value = lstFuncionario[i].Vvt5;
                    ws.Cells[p, 39].Value = lstFuncionario[i].Op6;
                    ws.Cells[p, 40].Value = lstFuncionario[i].Qvt6;
                    ws.Cells[p, 41].Value = lstFuncionario[i].Vvt6;
                    ws.Cells[p, 42].Value = lstFuncionario[i].somaGeral;
                    p++;
                }
                ws.Cells.AutoFitColumns();

                /* Pedido*/
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando Pedidos...");
                var wPedido = pck.Workbook.Worksheets.Add("Pedido");
                wPedido.View.ShowGridLines = true;

                wPedido.Cells[1, 1].Value = "Buscar_Item";
                wPedido.Cells[1, 2].Value = "Server = 1";
                wPedido.Cells[1, 3].Value = "Server";
                wPedido.Cells[1, 4].Value = "CNPJ";
                wPedido.Cells[1, 5].Value = "Unidade";
                wPedido.Cells[1, 6].Value = "Departamento";
                wPedido.Cells[1, 7].Value = "Matricula";
                wPedido.Cells[1, 8].Value = "Operadora";
                wPedido.Cells[1, 9].Value = "Qtd";
                wPedido.Cells[1, 10].Value = "Valor_Facial";
                wPedido.Cells[1, 11].Value = "Total";

                wPedido.Cells[1, 1, 1, 11].Style.Font.Bold = true;

                int PedidoCount = lstPedido.Count;
                int o = 2;
                for (int i = 0; i < PedidoCount; i++)
                {   //Console.WriteLine("[" + i + "] de [" + PedidoCount + "] - Pedidos");
                    wPedido.Cells[o, 1].Value = lstPedido[i].CNPJ + " - " + lstPedido[i].Unidade + " - " + lstPedido[i].Departamento + " - " + lstPedido[i].Matricula + " - ";
                    wPedido.Cells[o, 2].Value = lstPedido[i].CNPJ + " - " + lstPedido[i].Unidade + " - " + lstPedido[i].Departamento + " - " + lstPedido[i].Matricula;
                    //3
                    wPedido.Cells[o, 4].Value = lstPedido[i].CNPJ;
                    wPedido.Cells[o, 5].Value = lstPedido[i].Unidade;
                    wPedido.Cells[o, 6].Value = lstPedido[i].Departamento;
                    wPedido.Cells[o, 7].Value = lstPedido[i].Matricula;
                    wPedido.Cells[o, 8].Value = lstPedido[i].Operadora;
                    wPedido.Cells[o, 9].Value = lstPedido[i].Qtd;
                    wPedido.Cells[o, 10].Value = lstPedido[i].Valor_Facial;
                    wPedido.Cells[o, 11].Value = lstPedido[i].Total;
                    o++;
                }
                wPedido.Cells.AutoFitColumns();

                /* Operadora*/
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criando Pedidos...");
                var wOperadora = pck.Workbook.Worksheets.Add("Operadora");
                wOperadora.View.ShowGridLines = true;

                wOperadora.Cells[1, 1].Value = "Código";
                wOperadora.Cells[1, 2].Value = "Nome";

                wOperadora.Cells[1, 1, 1, 11].Style.Font.Bold = true;

                int Operadoras = lstOperadoras.Count;
                int u = 2;
                for (int i = 0; i < Operadoras; i++)
                {
                    wOperadora.Cells[u, 1].Value = lstOperadoras[i].Codigo;
                    wOperadora.Cells[u, 2].Value = lstOperadoras[i].Nome;
                    u++;
                }

                wOperadora.Cells.AutoFitColumns();

                pck.Workbook.Properties.Title = "Planilha de Dados";
                pck.Workbook.Properties.Author = string.Join(Environment.NewLine, "testeteste@techmex.com", "Guilherme Veiga");
                pck.Workbook.Properties.Company = "Tech. Mex";

                pck.Save();
                
                //frmMessage fMessage = new frmMessage("[EXCEL] - Criado com sucesso !");
                //fMessage.Show();                
                //frmMessage.ShowDialogMsg("[EXCEL] - Criado com sucesso"); // ("[" + DateTime.Now + "][EXCEL] - Criado com sucesso");
                //frm.EscreveNoConsole("[" + DateTime.Now + "][EXCEL] - Criado com sucesso");
            }
            catch (Exception erro)
            {
                Console.WriteLine(erro.Message);
            }

        }
    }
}
