﻿
namespace Conversor.Manager
{
    public class clsUnidade
    {
        public string Server;
        public string CNPJ;
        public string CodUnidade;
        public string Endereco;
        public string Bairro;
        public string Cidade;
        public string Cep;
        public string UF;
    }

    public class clsDepartamento
    {
        public string Server;
        public string CNPJ;
        public string CodUnidade;
        public string CodDepartamento;
        public string Departamento;
    }

    public class clsFuncionario
    {
        public string Server;
        public string CNPJ;
        public string Empresa;
        public string CodUnidade;
        public string CodDepartamento;
        public string Departamento;
        public string Endereco;
        public string Bairro;
        public string Cidade;
        public string UF;
        public string Cep;
        public string ID;
        public string Matricula;
        public string Funcionario;
        public string RG;
        public string CPF;
        public string Data_Nascimento;
        public string Nome_Mae;
        public string Sexo;
        public string Endereco_Residencial;
        public string Bairro_Residencial;
        public string Municipio_Residencial;
        public string Cep_Residencial;
        public string Op1; // Operadora
        public string Qvt1; // Quantidade
        public string Vvt1; // Valor
        public decimal soma1; // soma
        public string Op2;
        public string Qvt2;
        public string Vvt2;
        public decimal soma2; // soma
        public string Op3;
        public string Qvt3;
        public string Vvt3;
        public decimal soma3; // soma
        public string Op4;
        public string Qvt4;
        public string Vvt4;
        public decimal soma4; // soma
        public string Op5;
        public string Qvt5;
        public string Vvt5;
        public decimal soma5; // soma
        public string Op6;
        public string Qvt6;
        public string Vvt6;
        public decimal soma6; // soma
        public string Op7;
        public string Qvt7;
        public string Vvt7;
        public decimal soma7; // soma
        public string Op8;
        public string Qvt8;
        public string Vvt8;
        public decimal soma8; // soma
        public decimal somaGeral; // soma
    }

    public class clsPedido
    {
        public string Buscar_Item;
        public string Server;
        public string CNPJ;
        public string Unidade;
        public string Departamento;
        public string Matricula;
        public string Operadora;
        public string Qtd;
        public string Valor_Facial;
        public decimal Total;
    }

    public class clsPedidoAntigos
    {
        public string Matricula;
        public string Operadora;
    }

    public class clsProcura
    {
        public string Matricula;
    }

    public class clsNovosColaboradores
    {
        public string Matricula;
        public string Colaborador;
        public string Operadora = "Operadora Não Encontrado";
        public string Situacao;
        public string RG;
        public string CPF;
        public string Data_Nascimento;
        public string Endereco_Residencial = "Endereço Não Encontrado";
        public string Sic = "Sic Não Encontrado";
    }
    
    public class clsColaboradoresAntigos
    {
        public string Matricula;
        public string Colaborador;
        public string Operadora = "Operadora Não Encontrado";
        public string Situacao;
        public string RG;
        public string CPF;
        public string Data_Nascimento;
        public string Endereco_Residencial = "Endereço Não Encontrado";
        public string Sic = "Sic Não Encontrado";


        public string Op1; // Operadora
        public string Qvt1; // Quantidade
        public string Vvt1; // Valor
        public decimal soma1; // soma
        public string Op2;
        public string Qvt2;
        public string Vvt2;
        public decimal soma2; // soma
        public string Op3;
        public string Qvt3;
        public string Vvt3;
        public decimal soma3; // soma
        public string Op4;
        public string Qvt4;
        public string Vvt4;
        public decimal soma4; // soma
        public string Op5;
        public string Qvt5;
        public string Vvt5;
        public decimal soma5; // soma
        public string Op6;
        public string Qvt6;
        public string Vvt6;
        public decimal soma6; // soma
        public string Op7;
        public string Qvt7;
        public string Vvt7;
        public decimal soma7; // soma
        public string Op8;
        public string Qvt8;
        public string Vvt8;
        public decimal soma8; // soma
        public decimal somaGeral; // soma
    }
    

    public class clsOperadorasCadastradas
    {
        public string Codigo;
        public string Nome;
        public string Empresa;
    }

    public class clsOperadoras
    {
        public string Codigo;
        public string Nome;
    }

    public class clsOperadorasNaoCadastradas
    {
        public string Codigo;
        public string Nome;
        public string Empresa;
        public string Situacao;
    }

    public class clsHeader
    {
        public string ValorArquivo;
    }
}
