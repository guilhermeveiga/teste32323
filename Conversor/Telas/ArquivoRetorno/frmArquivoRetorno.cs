﻿using Global;
using OfficeOpenXml;
using System;
using System.IO;
using System.Windows.Forms;

namespace Conversor.Telas.ArquivoRetorno
{
    public partial class frmArquivoRetorno : SombraAereo
    {
        private string sArquivoRetorno;

        /* Retirar daqui */
        public class clsArquivoRetorno
        {

        }

        public frmArquivoRetorno()
        {
            InitializeComponent();
        }

        private void frmArquivoRetorno_Load(object sender, EventArgs e)
        {

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {

        }

        private void lblArquivoRetorno_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd1 = new OpenFileDialog();

            ofd1.Multiselect = true;
            ofd1.Title = "Selecione um Arquivo";
            ofd1.Filter = "Excel (*.xlsx)|*.xlsx";
            ofd1.InitialDirectory = @"C:\";
            ofd1.CheckFileExists = true;
            ofd1.CheckPathExists = true;
            ofd1.FilterIndex = 2;
            ofd1.RestoreDirectory = true;
            ofd1.ReadOnlyChecked = true;
            ofd1.ShowReadOnly = true;

            if (ofd1.ShowDialog() == DialogResult.OK)
            {
                sArquivoRetorno = ofd1.FileName.ToString();
            }
            lerArquivoRetorno();
        }


        private void lerArquivoRetorno()
        {
            FileInfo newFile = new FileInfo(sArquivoRetorno);
            var package = new ExcelPackage(newFile);
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
            
            for (int i = 3; i <= workSheet.Dimension.End.Row; i++)// cria corpo
            {
                object Matricula = workSheet.Cells[i, 1].Value;
                object Colaborador = workSheet.Cells[i, 2].Value;
                object Operadora = workSheet.Cells[i, 3].Value;
                object Situacao = workSheet.Cells[i, 4].Value;
                object RG = workSheet.Cells[i, 5].Value;
                object CPF = workSheet.Cells[i, 6].Value;
                object Endereco = workSheet.Cells[i, 7].Value;
                object Sic = workSheet.Cells[i, 8].Value;
                bunifuCustomDataGrid1.Rows.Add(Matricula, Colaborador, Operadora, Situacao, RG, CPF, Endereco, Sic);
            }
        }
    }
}
